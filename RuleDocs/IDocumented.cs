﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
namespace RuleDocs
{
	/// <summary>Interface specifiying that a class has documentation generation capabilities.</summary>
	public interface IDocumented
	{
		/// <summary>Creates an <see cref="RuleData"/> instance from the
		/// internal info of the class implementing the <see cref=" IDocumented"/>
		/// interface.</summary>
		/// <returns>An instance of <see cref="RuleData"/>.</returns>
		RuleData CreateDocumentation();
	}
}
