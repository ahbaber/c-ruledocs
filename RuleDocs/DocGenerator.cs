﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RuleDocs
{
	/// <summary>Gathers all of the document data from an <see cref="IDocumented"/>
	/// instance and its children, and produces and generates documentation as a
	/// read only collection of strings.</summary>
	public class DocGenerator
	{
		/// <summary>Initializes a new instance of the <see cref="T:RuleDocs.DocGenerator"/> class.</summary>
		/// <param name="formatter">The injected <see cref="Formatter"/> that will
		/// handle all formatting details..</param>
		public DocGenerator(Formatter formatter = null)
		{
			_formatter = formatter ?? new Formatter();
		}

		/// <summary>Extracts document data from the <see cref="IDocumented"/> root, and all of its sub hierarchy.</summary>
		/// <returns>A read only collection of strings containing document information.</returns>
		/// <param name="rootRule">The root rule to have its documentation data extracted.</param>
		public ReadOnlyCollection<string> FormatRules(IDocumented rootRule)
		{
			var rule = rootRule.CreateDocumentation();
			_formatter.SetMaxIndent(rule.Depth)
				.SetHeaderKeys(rule.GetAllKeys());

			string header = _formatter.GetHeader();
			var docs = header != null
				? new List<string> { header }
				: new List<string>();
			CreateDocs(docs, rule, 0);
			return new ReadOnlyCollection<string>(docs);
		}

		private void CreateDocs(List<string> docs, RuleData rule, int indent)
		{
			string documentLine = _formatter.FormatLine(rule, indent);
			if (documentLine != null)
			{ docs.Add(documentLine); }

			if (rule.HasChildren)
			{
				++indent;
				foreach (var subrule in rule.Children)
				{
					CreateDocs(docs, subrule, indent);
				}
			}
		}

		private readonly Formatter _formatter;
	}
}
