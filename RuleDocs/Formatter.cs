﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;
using System.Text;

namespace RuleDocs
{
	/// <summary>This class formats the documentation data into CVS values.</summary>
	public class Formatter
	{
		/// <summary>Initializes a new instance of the <see cref="T:RuleDocs.Formatter"/> class.</summary>
		/// <param name="comma">Optional string used to delimit each piece of data.
		/// Defaults to ",".</param>
		/// <param name="quote">Optional string indicating how fields should be wrapped if necessary.
		/// Defaults to "\"".</param>
		/// <param name="dataQuote">Optional string indicating how existing double quotes should be replaced in
		/// in the event that there is a conflict with <see cref="Quote"/>.  Default value is "“".</param>
		/// <param name="newLine">Optional string to indicate the end of a line of documetation.
		/// Default is system dependent.</param>
		public Formatter(string comma = null, string quote = null, string dataQuote = null, string newLine = null)
		{
			Comma = comma ?? ",";
			Quote = quote ?? "\"";
			DataQuote = dataQuote ?? "“";
			NewLine = newLine ?? Environment.NewLine;
		}

		/// <summary>Sets the amount that the most indented line will be indented by.</summary>
		/// <returns>The current instance of <see cref="Formatter"/>.</returns>
		/// <param name="maxIndent">Max indent.</param>
		public Formatter SetMaxIndent(int maxIndent)
		{
			_maxIndent = maxIndent;
			BuildCache(maxIndent);
			return this;
		}

		/// <summary>Sets the keys that will be used to create the header line,
		/// as well as extract the values from the documentation.</summary>
		/// <returns>The current instance of <see cref="Formatter"/>.</returns>
		/// <param name="keys">The keys created form the documentation data.</param>
		public virtual Formatter SetHeaderKeys(IEnumerable<string> keys)
		{
			_headerKeys = keys;
			return this;
		}

		/// <summary>Default: retrieves a formatted header line.  Each unique key in the
		/// data will be given a separate column in the header row.  They will
		/// be written out in alphabetical order.  This may be overridden to generate a
		/// custom ordering.</summary>
		/// <returns>The formatted header.</returns>
		public virtual string GetHeader()
		{
			_builder.Clear();
			_builder.Append($"Name{PostIndent(0)}{Comma}Description");
			foreach (var header in _headerKeys)
			{
				string formattedHeader = FormatData(header);
				_builder.Append($"{Comma}{formattedHeader}");
			}
			_builder.Append(NewLine);
			return _builder.ToString();
		}

		/// <summary>Formats one line of documentation.  Each row will have
		/// one cell for name, description, and each of the aggregate keys.  This
		/// may be overridden to provide custom formatting.</summary>
		/// <returns>A formatted string.</returns>
		/// <param name="rule">The <see cref="RuleData"/> with the raw data.</param>
		/// <param name="indent">How much the data needs to be indented.</param>
		public virtual string FormatLine(RuleData rule, int indent)
		{
			_builder.Clear();
			string pre = PreIndent(indent);
			string post = PostIndent(indent);
			string name = FormatData(rule.Name);
			string description = FormatData(rule.Description);
			_builder.Append($"{pre}{name}{post}{Comma}{description}");
			foreach (var header in _headerKeys)
			{
				string value = FormatData(rule.GetValue(header));
				_builder.Append($"{Comma}{value}");
			}
			_builder.Append(NewLine);
			return _builder.ToString();
		}

		/// <summary>Formats a single data cell such that any delimiter characters
		/// are escaped for proper handling.  This may be overriden to provide custom behavior.</summary>
		/// <returns>CVS safe data.</returns>
		/// <param name="data">The raw data prior to escaping.</param>
		public virtual string FormatData(string data)
		{
			data = data.Replace(Quote, DataQuote);
			if (data.IndexOf(Comma, StringComparison.InvariantCulture) > -1
			|| data.IndexOf("\n", StringComparison.InvariantCulture) > -1
			|| data.IndexOf("\r", StringComparison.InvariantCulture) > -1
			|| data.IndexOf(NewLine, StringComparison.InvariantCulture) > -1)
			{
				data = $"{Quote}{data}{Quote}";
			}
			return data;
		}

		/// <summary>Contains a separator string to be used to delimit each piece of data.</summary>
		/// <value>The seprator string.</value>
		protected string Comma { get; }

		/// <summary>Contains the string used to mark the end of each data row.</summary>
		/// <value>The new line string.</value>
		protected string NewLine { get; }

		/// <summary>Contains the string used to delimit each piece of data that has other formatting characters.</summary>
		/// <value>The quote string.</value>
		protected string Quote { get; }

		/// <summary>Contains the string to replace regular quotes in the data.</summary>
		/// <value>The data quote string.</value>
		protected string DataQuote { get; }

		/// <summary>Retrieves a set of separators (as from <seealso cref="Comma"/>) to be placed in the front of the formatted string.</summary>
		/// <value>The pre indent string.</value>
		protected string PreIndent(int indent)
		{
			indent = Math.Max(indent, 0);
			if (indent >= _commaCache.Count)
			{ BuildCache(indent); }
			return _commaCache[indent];
		}

		/// <summary>Retrieves a set of separators (as from <seealso cref="Comma"/>) to be placed after the first element in the formatted string.</summary>
		/// <value>The post indent string.</value>
		protected string PostIndent(int indent) { return PreIndent(_maxIndent - indent); }

		/// <summary>An internal <see cref="StringBuilder"/> used to format data.
		/// Care sjhouod always be taken to call _builder.Clear() prior to use.</summary>
		protected StringBuilder _builder = new StringBuilder();

		private void BuildCache(int maxPadding)
		{
			string value = _commaCache[_commaCache.Count - 1];
			while (_commaCache.Count <= maxPadding)
			{
				value += Comma;
				_commaCache.Add(value);
			}
		}

		private int _maxIndent;
		private IEnumerable<string> _headerKeys;
		private List<string> _commaCache = new List<string> { "" };
	}
}
