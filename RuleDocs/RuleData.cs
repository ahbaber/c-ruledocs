﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace RuleDocs
{
	/// <summary>Contains the unformatted data extracted from a class for the
	/// purposes of generating data about the class.</summary>
	public class RuleData
	{
		/// <summary>Constructs a new instance of the <see cref="T:RuleDocs.AbstractDocumentationData"/> class.</summary>
		/// <returns>The constructed <see cref="RuleData"/>.</returns>
		/// <param name="name">The name given to the rule.</param>
		/// <param name="description">The description given to the rule.</param>
		/// <param name="caller">The class whose data was used to construct the <see cref="RuleData"/>.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public static RuleData Construct<T>(string name, string description, T caller)
		{
			return new RuleData(name, description).SetNames(caller);
		}

		/// <summary>The name / identifier of the rule or info</summary>
		/// <value>Name / Identifier.</value>
		public string Name { get; }

		/// <summary>More deatiled information about the rule.</summary>
		/// <value>Information about the rule.</value>
		public string Description { get; }

		/// <summary>Adds a key value pair to the DataMap.  If the key value pair
		/// already exists, then the new key value will overwrite it.  The value will
		/// be converted to a string.</summary>
		/// <returns>The child instance of <see cref="RuleData"/>.</returns>
		/// <param name="key">The key the value will be associated with.</param>
		/// <param name="value">The value that will be stored.</param>
		/// <typeparam name="T">Any type implementing ToString().</typeparam>
		public RuleData AddData<T>(string key, T value)
		{
			var data = _data ?? CreateData();
			if (data.ContainsKey(key))
			{ data[key] = value.ToString(); }
			else
			{ data.Add(key, value.ToString()); }
			return this;
		}

		/// <summary>Retrieves all data keys from the current node and all of its
		/// child rules.  The keys will will be in alphabetical order.  The default
		/// Name and Description fields will not be inlcuded in the collection.</summary>
		/// <returns>The A read only collection of keys.</returns>
		public ReadOnlyCollection<string> GetAllKeys()
		{
			if (_roMetaKeys == null)
			{
				if (_metaKeys == null)
				{
					var keys = new SortedSet<string>();
					PopulateMetaKeys(keys);
				}
				_roMetaKeys = new ReadOnlyCollection<string>(_metaKeys.ToList());
			}
			return _roMetaKeys;
		}

		/// <summary>Retrieves a value associated with a key from the the data map.
		/// If the key does not exist, then an empty string will be returned.</summary>
		/// <returns>The value associated with <paramref name="key"/>, or an empty string.</returns>
		/// <param name="key">The Key associated with the value.</param>
		public string GetValue(string key)
		{
			if (_data == null) return string.Empty;
			return _data.TryGetValue(key, out string value) ? value : string.Empty;
		}

		/// <summary>Checks to see if there are subrules owned by the current rule.</summary>
		/// <value><c>true</c> if there are children; otherwise, <c>false</c>.</value>
		public bool HasChildren { get { return _children != null && _children.Count > 0; } }

		/// <summary>Retrieves any child RuleData elements if present.  If there
		/// is no children, then null will be returned.</summary>
		/// <value>Null or a read only collection of child nodes.</value>
		public ReadOnlyCollection<RuleData> Children { get; private set; }

		/// <summary>Adds a subrule to the hierarchy.</summary>
		/// <returns>The child instance of <see cref="RuleData"/>.</returns>
		/// <param name="rule">The rule that will have its info added to the hiearchy.</param>
		public RuleData AddChild(IDocumented rule)
		{
			var subrule = rule.CreateDocumentation();
			return AddChildRuleData(subrule);
		}

		/// <summary>Generic version that cataches classes not implementing RuleDocs.</summary>
		/// <returns>The child instance of <see cref="RuleData"/>.</returns>
		/// <param name="rule">The rule that will have its (lack of) info added to the hiearchy.</param>
		/// <typeparam name="T">Any type not implementing <see cref="IDocumented"/>.</typeparam>
		public RuleData AddChild<T>(T rule)
		{
			if (rule is IDocumented implemented)
			{
				return AddChild(implemented);
			}

			var subrule = new RuleData("Unimplemented", "This class has not implemented and documentation")
				.SetNames(rule);
			subrule.AddData("MissingDocType", subrule.FullTypeName);
			return AddChildRuleData(subrule);
		}

		/// <summary>Retrieves the depth of the deepest child node in the hierarchy.</summary>
		/// <value>Int indicting the depth of the child hiearchy.</value>
		public int Depth { get; private set; }

		/// <summary>The name of the class used to generate the <see cref="RuleData"/>.</summary>
		/// <value>The name of the class type.</value>
		public string TypeName { get; private set; }

		/// <summary>The namespace and name of the class used to generate the <see cref="RuleData"/>.</summary>
		/// <value>The namespace and name of the class type.</value>
		public string FullTypeName { get; private set; }

		private RuleData(string name, string description)
		{
			Name = name;
			Description = description;
		}

		private Dictionary<string, string> CreateData()
		{
			_data = new Dictionary<string, string>();
			return _data;
		}

		private RuleData SetNames<T>(T t)
		{
			var type = typeof(T);
			TypeName = type.Name;
			FullTypeName = type.FullName;
			return this;
		}

		private List<RuleData> CreateList()
		{
			_children = new List<RuleData>();
			Children = new ReadOnlyCollection<RuleData>(_children);
			return _children;
		}

		private void PopulateMetaKeys(SortedSet<string> metaKeys)
		{
			_metaKeys = metaKeys;
			_roMetaKeys = null;
			if (_data != null)
			{
				_metaKeys.UnionWith(_data.Keys);
			}
			if (HasChildren)
			{
				foreach (var child in Children)
				{
					child.PopulateMetaKeys(metaKeys);
				}
			}
		}

		private RuleData AddChildRuleData(RuleData subrule)
		{
			(_children ?? CreateList()).Add(subrule);

			Depth = Math.Max(Depth, subrule.Depth + 1);

			return subrule;
		}

		private Dictionary<string, string> _data;
		private SortedSet<string> _metaKeys;
		private ReadOnlyCollection<string> _roMetaKeys;
		private List<RuleData> _children;
	}
}
