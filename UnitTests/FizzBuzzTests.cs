﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using UnitTests.FizzBuzzRuleSet;
using UnitTests.FizzBuzzRuleSet.Rules;
using UnitTests.FizzBuzzRuleSet.Conditions.Builder;
using UnitTests.FizzBuzzRuleSet.Conditions.Value;
using UnitTests.FizzBuzzRuleSet.Formatters;
using Xunit;

namespace UnitTests
{
	/// <summary>These integration tests are not directly relevant to RuleDocs.
	/// However, they do verify that the completely overwrought version of FizzBuzz
	/// is actually working.</summary>
	public class FizzBuzzTests
	{
		private FizzBuzz ConstructFizzBuss()
		{
			return new FizzBuzz(new BuilderCheckRule(new BuilderIsEmpty(), new Concat<int>()))
				.AddRule(new KeywordRule("Fizz", new ValueIsMultipleOf(3), new Concat<string>()))
				.AddRule(new KeywordRule("Buzz", new ValueIsMultipleOf(5), new Concat<string>()));
		}

		[Theory]
		[InlineData(1)]
		[InlineData(2)]
		public void Transform_ReturnsNumbers(int n)
		{
			var fizzBuzz = ConstructFizzBuss();

			string expected = n.ToString();
			string actual = fizzBuzz.Transform(n);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(3)]
		[InlineData(6)]
		[InlineData(9)]
		[InlineData(12)]
		public void Transform_FizzReturned(int n)
		{
			var fizzBuzz = ConstructFizzBuss();

			string expected = "Fizz";
			string actual = fizzBuzz.Transform(n);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(5)]
		[InlineData(10)]
		[InlineData(20)]
		[InlineData(25)]
		public void Transform_BuzzReturned(int n)
		{
			var fizzBuzz = ConstructFizzBuss();

			string expected = "Buzz";
			string actual = fizzBuzz.Transform(n);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(15)]
		[InlineData(30)]
		[InlineData(45)]
		public void Transform_FizzBuzzReturned(int n)
		{
			var fizzBuzz = ConstructFizzBuss();

			string expected = "FizzBuzz";
			string actual = fizzBuzz.Transform(n);

			Assert.Equal(expected, actual);
		}
	}
}
