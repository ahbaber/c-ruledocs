﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using Xunit;

namespace UnitTests
{
	public class RuleDataTests
	{
		[Fact]
		public void NameAndDescriptionFields_ReturnsBasicData()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this);

			Assert.Equal("TestName", doc.Name);
			Assert.Equal("TestDescription", doc.Description);

			Assert.Equal("RuleDataTests", doc.TypeName);
			Assert.Equal("UnitTests.RuleDataTests", doc.FullTypeName);
		}

		[Fact]
		public void GetAllKeys_NoData_ReturnsEmptyCollection()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this);

			Assert.Empty(doc.GetAllKeys());
		}

		[Fact]
		public void GetAllKeys_HasData_ReturnsCollection()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this)
				.AddData("key", "value");

			Assert.Single(doc.GetAllKeys());
			Assert.Contains("key", doc.GetAllKeys());
		}

		[Fact]
		public void GetValue_KeyDoesNotExist_ReturnsEmptyString()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this);

			Assert.Equal("", doc.GetValue("NoKey"));
		}

		[Fact]
		public void GetValue_KeyExists_ReturnsKey()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this)
				.AddData("key", "value");

			Assert.Equal("value", doc.GetValue("key"));
		}

		[Fact]
		public void Depth_NoChildren_Returns0()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this);

			Assert.Equal(0, doc.Depth);
		}

		[Fact]
		public void Depth_HasChild_ReturnsExpectedDepths()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this);
			doc.AddChild(new Subrule("Child"));

			Assert.Equal(1, doc.Depth);
		}

		[Fact]
		public void HasChildren_NoChild_ReturnsFalse()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this);

			Assert.False(doc.HasChildren);
		}

		[Fact]
		public void HasChildren_OneChild_ReturnsFalse()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this);
			doc.AddChild(new Subrule("Child"));

			Assert.True(doc.HasChildren);
		}

		[Fact]
		public void Children_NoChildren_ReturnsNull()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this);

			Assert.Null(doc.Children);
		}

		[Fact]
		public void Children_HasChildren_ReturnsCollection()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this);
			doc.AddChild(new Subrule("Child"));

			Assert.Single(doc.Children);
		}

		[Fact]
		public void GetAllKeys_ChildAndParentWithDifferentKeys_ReturnsExpectedKeys()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this)
				.AddData("key", "value");
			doc.AddChild(new SubruleWithData("sub"));

			var actual = doc.GetAllKeys();

			Assert.Equal(2, actual.Count);
		}

		[Fact]
		public void GetValue_ChildAndParentWithDifferentKeys_ReturnsExpectedValues()
		{
			var doc = RuleData.Construct("TestName", "TestDescription", this)
				.AddData("key", "value");
			doc.AddChild(new SubruleWithData("sub"));
			var child = doc.Children[0];

			Assert.Equal("value", doc.GetValue("key"));
			Assert.Equal("", doc.GetValue("subKey"));
			Assert.Equal("", child.GetValue("key"));
			Assert.Equal("subValue", child.GetValue("subKey"));
		}

		private class Subrule : IDocumented
		{
			public Subrule(string name)
			{
				_name = name;
			}
			public RuleData CreateDocumentation()
			{
				return RuleData.Construct(_name, "Subrule Description", this);
			}
			private readonly string _name;
		}

		private class SubruleWithData : IDocumented
		{
			public SubruleWithData(string name)
			{
				_name = name;
			}
			public RuleData CreateDocumentation()
			{
				return RuleData.Construct(_name, "Subrule2 Description", this)
					.AddData("subKey", "subValue");
			}
			private readonly string _name;
		}
	}
}
