﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;
using RuleDocs;

namespace UnitTests.FizzBuzzRuleSet.Formatters
{
	public class Concat<T> : IFormatter<T>
	{
		public StringBuilder Format(StringBuilder builder, T value)
		{
			return builder.Append(value);
		}

		public RuleData CreateDocumentation()
		{
			string typeName = typeof(T).Name;
			return RuleData.Construct($"Concat<{typeName}>",
				"Appends values to the end of the StringBuilder", this)
				.AddData("RuleType", $"IFormatter<{typeName}>")
				.AddData("FormatterType", "Concat")
				.AddData("Input", $"{typeName}")
				.AddData("Output", "StringBuilder");
		}
	}
}
