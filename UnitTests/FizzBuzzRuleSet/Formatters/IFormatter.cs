﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Text;

namespace UnitTests.FizzBuzzRuleSet.Formatters
{
	public interface IFormatter<T> : IDocumented
	{
		StringBuilder Format(StringBuilder builder, T value);
	}
}
