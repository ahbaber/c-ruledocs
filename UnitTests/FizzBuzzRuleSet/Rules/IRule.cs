﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Text;

namespace UnitTests.FizzBuzzRuleSet.Rules
{
	public interface IRule : IDocumented
	{
		StringBuilder Transform(StringBuilder builder, int n);
	}
}
