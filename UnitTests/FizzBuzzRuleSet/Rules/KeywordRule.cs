﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using UnitTests.FizzBuzzRuleSet.Conditions;
using UnitTests.FizzBuzzRuleSet.Formatters;
using System.Text;
using RuleDocs;

namespace UnitTests.FizzBuzzRuleSet.Rules
{
	public class KeywordRule : IRule
	{
		public KeywordRule(string keyword, IConditionValue condition, IFormatter<string> formatter)
		{
			_keyword = keyword;
			_condition = condition;
			_formatter = formatter;
		}

		public StringBuilder Transform(StringBuilder builder, int n)
		{
			if (_condition.Check(n))
			{
				builder = _formatter.Format(builder, _keyword);
			}
			return builder;
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Keyword Rule",
				"Will insert the keyword into the formatter if the condition is met.", this)
				.AddData("Keyword", _keyword)
				.AddData("RuleType", "IRule with IConditionValue and IFormatter<String>")
				.AddData("Input", "StringBuilder int")
				.AddData("Output", "StringBuilder");
			docs.AddChild(_condition).AddData("Parent", $"KeywordRule({_keyword})");
			docs.AddChild(_formatter).AddData("Parent", $"KeywordRule({_keyword})");
			return docs;
		}

		private readonly string _keyword;
		private readonly IConditionValue _condition;
		private readonly IFormatter<string> _formatter;
	}
}
