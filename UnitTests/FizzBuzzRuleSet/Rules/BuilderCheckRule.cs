﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using UnitTests.FizzBuzzRuleSet.Conditions;
using UnitTests.FizzBuzzRuleSet.Formatters;
using System.Text;
using RuleDocs;

namespace UnitTests.FizzBuzzRuleSet.Rules
{
	public class BuilderCheckRule : IRule
	{
		public BuilderCheckRule(IConditionBuilder condition, IFormatter<int> formatter)
		{
			_condition = condition;
			_formatter = formatter;
		}

		public StringBuilder Transform(StringBuilder builder, int n)
		{
			if (_condition.Check(builder))
			{
				_formatter.Format(builder, n);
			}
			return builder;
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("Builder Check Rule",
				"Will insert the passed in value if the builder condition is met", this)
				.AddData("RuleType", "IRule with IConditionBuilder and IFormatter<int>")
				.AddData("Input", "StringBuilder int")
				.AddData("Output", "StringBuilder");
			docs.AddChild(_condition).AddData("Parent", "BuilderCheckRule");
			docs.AddChild(_formatter).AddData("Parent", "BuilderCheckRule");
			return docs;
		}

		private IConditionBuilder _condition;
		private readonly IFormatter<int> _formatter;
	}
}
