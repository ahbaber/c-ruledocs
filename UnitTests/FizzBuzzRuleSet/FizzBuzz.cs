﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Collections.Generic;
using System.Text;
using UnitTests.FizzBuzzRuleSet.Rules;

namespace UnitTests.FizzBuzzRuleSet
{
	public class FizzBuzz : IDocumented
	{
		public FizzBuzz(IRule defaultRule)
		{
			_defaultRule = defaultRule;
		}

		public FizzBuzz AddRule(IRule rule)
		{
			_rules.Add(rule);
			return this;
		}

		public string Transform(int n)
		{
			StringBuilder output = new StringBuilder();
			foreach (var rule in _rules)
			{
				output = rule.Transform(output, n);
			}
			return output.Length > 0
				? output.ToString()
				: n.ToString();
		}

		public RuleData CreateDocumentation()
		{
			var docs = RuleData.Construct("FizzBuzz",
				"Rules for processing FizzBuzz", this)
				.AddData("RuleType", "Container for IRules");
			for (int i = 0; i < _rules.Count; ++i)
			{
				docs.AddChild(_rules[i])
					.AddData("Parent", "FizzBuzz")
					.AddData("RuleOrder", i + 1);
			}
			docs.AddChild(_defaultRule)
				.AddData("Parent", "FizzBuzz")
				.AddData("RuleOrder", _rules.Count + 1);
			return docs;
		}

		private readonly IRule _defaultRule;
		private readonly List<IRule> _rules = new List<IRule>();
	}
}
