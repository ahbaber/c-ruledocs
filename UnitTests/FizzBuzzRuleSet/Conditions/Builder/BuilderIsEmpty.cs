﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Text;

namespace UnitTests.FizzBuzzRuleSet.Conditions.Builder
{
	public class BuilderIsEmpty : IConditionBuilder
	{
		public bool Check(StringBuilder builder)
		{
			return builder.Length == 0;
		}

		public RuleData CreateDocumentation()
		{
			return RuleData.Construct("BuilderIsEmpty",
				"Checks if the StringBuilder is empty", this)
				.AddData("RuleType", "IConditionBuilder")
				.AddData("ConditionType", "Builder Length")
				.AddData("Input", "StringBuilder")
				.AddData("Output", "true/false")
				.AddData("ReturnValue", "true if the builder is empty");
		}
	}
}
