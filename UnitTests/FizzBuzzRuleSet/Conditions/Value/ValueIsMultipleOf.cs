﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;

namespace UnitTests.FizzBuzzRuleSet.Conditions.Value
{
	public class ValueIsMultipleOf : IConditionValue
	{
		public ValueIsMultipleOf(int mod)
		{
			_mod = mod;
		}

		public bool Check(int n)
		{
			return n % _mod == 0;
		}

		public RuleData CreateDocumentation()
		{
			return RuleData.Construct("ValueIsMultipleOf",
				$"Checks to see if the value is a multiple of {_mod}",
				this)
				.AddData("RuleType", "IConditionValue")
				.AddData("ConditionType", "Modulus")
				.AddData("Input", "int")
				.AddData("Output", "true/false")
				.AddData("ReturnValue", $"true if value is a multiple of {_mod}");
		}

		private readonly int _mod;
	}
}
