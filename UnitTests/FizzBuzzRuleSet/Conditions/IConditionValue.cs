﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;

namespace UnitTests.FizzBuzzRuleSet.Conditions
{
	public interface IConditionValue : IDocumented
	{
		bool Check(int n);
	}
}
