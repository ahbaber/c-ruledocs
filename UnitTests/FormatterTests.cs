﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System;
using Xunit;

namespace UnitTests
{
	public class FormatterTests
	{
		[Fact]
		public void GetHeader_RuleNoData_ReturnesExpectedHeader()
		{
			var formatter = new Formatter();
			var rule = new TestRule();
			var ruleData = rule.CreateDocumentation();

			string actual = formatter.SetHeaderKeys(ruleData.GetAllKeys())
				.GetHeader();

			Assert.Equal($"Name,Description{Environment.NewLine}", actual);
		}

		[Fact]
		public void GetHeader_RuleWithData_ReturnesExpectedHeader()
		{
			var formatter = new Formatter();
			var rule = new TestRuleWithData();
			var ruleData = rule.CreateDocumentation();

			string actual = formatter.SetHeaderKeys(ruleData.GetAllKeys())
				.GetHeader();

			Assert.Equal($"Name,Description,Field1,Field2{Environment.NewLine}", actual);
		}

		[Fact]
		public void GetHeader_PostPadded_ReturnesExpectedHeader()
		{
			var formatter = new Formatter().SetMaxIndent(4);
			var rule = new TestRule();
			var ruleData = rule.CreateDocumentation();

			string actual = formatter.SetHeaderKeys(ruleData.GetAllKeys())
				.GetHeader();

			Assert.Equal($"Name,,,,,Description{Environment.NewLine}", actual);
		}

		[Theory]
		[InlineData("Field1,withcomma", false)]
		[InlineData("Field1\nwith new line", false)]
		[InlineData("Field1\rwith return line", false)]
		[InlineData("\"Field1 with quotes\"", true)]
		public void GetHeader_FieldsWithIllegalValues_ReturnsExpectedHeader(string field, bool useDataQuote)
		{
			var formatter = new Formatter();
			var rule = new TestRuleWithData { Field1 = field };
			var ruleData = rule.CreateDocumentation();
			string q = "\"";
			if (useDataQuote)
			{
				q = "";
				field = field.Replace('"', '“');
			}

			string expected = $"Name,Description,{q}{field}{q},Field2\n";
			string actual = formatter.SetHeaderKeys(ruleData.GetAllKeys())
				.GetHeader();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void FormatLine_SingleRule_ReturnsExpectedData()
		{
			var formatter = new Formatter();
			var rule = new TestRule();
			var ruleData = rule.CreateDocumentation();

			string actual = formatter.SetHeaderKeys(ruleData.GetAllKeys())
				.FormatLine(ruleData, 0);

			Assert.Equal($"TestRule,Test Description{Environment.NewLine}", actual);
		}

		[Fact]
		public void FormatLine_SingleRuleWithData_ReturnsExpectedData()
		{
			var formatter = new Formatter();
			var rule = new TestRuleWithData();
			var ruleData = rule.CreateDocumentation();

			string actual = formatter.SetHeaderKeys(ruleData.GetAllKeys())
				.FormatLine(ruleData, 0);

			Assert.Equal($"TestRule,Test Description,hello,7{Environment.NewLine}", actual);
		}

		[Fact]
		public void FormatLine_Indented_ReturnsExpectedData()
		{
			var formatter = new Formatter();
			var rule = new TestRule();
			var ruleData = rule.CreateDocumentation();

			string actual = formatter.SetHeaderKeys(ruleData.GetAllKeys())
				.FormatLine(ruleData, 4);

			Assert.Equal($",,,,TestRule,Test Description{Environment.NewLine}", actual);
		}

		[Fact]
		public void FormatLine_PostPadded_ReturnsExpectedData()
		{
			var formatter = new Formatter().SetMaxIndent(4);
			var rule = new TestRule();
			var ruleData = rule.CreateDocumentation();

			string actual = formatter.SetHeaderKeys(ruleData.GetAllKeys())
				.FormatLine(ruleData, 0);

			Assert.Equal($"TestRule,,,,,Test Description{Environment.NewLine}", actual);
		}

		[Theory]
		[InlineData("a", "a")] // no change
		[InlineData("\"abc\"", "“abc“")] // quotes replaced with data quotes
		[InlineData("1,2", "\"1,2\"")] // comma means it has to be quoted
		[InlineData("abc\ncde", "\"abc\ncde\"")] // new line menas it has to be quoted
		[InlineData("abc\rcde", "\"abc\rcde\"")] // return means it has to be quoted
		[InlineData("\"1,2\"", "\"“1,2“\"")] // quotes replaced with data quotes, also quotes added due to comma
		public void FormatData_ReturnsExpectedData(string data, string expected)
		{
			var formatter = new Formatter();

			string actual = formatter.FormatData(data);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("a", "a")]
		[InlineData("1,2", "1,2")] // comma not special, no change
		[InlineData("1\t2", "'1\t2'")] // quoted
		[InlineData("abc\ncde", "'abc\ncde'")] // quoted
		[InlineData("abc\rcde", "'abc\rcde'")] // quoted
		[InlineData("abc!cde", "abc!cde")] // single ! not special, no change
		[InlineData("abc!!cde", "'abc!!cde'")] // !! is a new line, so quoted
		[InlineData("'1,2'", "<!>1,2<!>")] // ' is replaced with <!>
		public void FormatData_CustomFormatter_ReturnsExpectedData(string data, string expected)
		{
			// tab is a common delimiter, but the other values are arbitrary
			var formatter = new Formatter("\t", "'", "<!>", "!!");

			string actual = formatter.FormatData(data);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("RuleName,withcomma", false)]
		[InlineData("RuleName\nwith new line", false)]
		[InlineData("RuleName\rwith return line", false)]
		[InlineData("\"RuleName with quotes\"", true)]
		public void FormatLine_NameWithIllegalValues_ReturnsExpectedData(string name, bool useDataQuote)
		{
			var formatter = new Formatter();
			var rule = new TestRuleWithData { Name = name };
			var ruleData = rule.CreateDocumentation();
			string q = "\"";
			if (useDataQuote)
			{
				q = "";
				name = name.Replace('"', '“');
			}

			string expected = $"{q}{name}{q},Test Description,hello,7{Environment.NewLine}";
			string actual = formatter.SetHeaderKeys(ruleData.GetAllKeys())
				.FormatLine(ruleData, 0);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("Description,withcomma", false)]
		[InlineData("Description\nwith new line", false)]
		[InlineData("Description\rwith return line", false)]
		[InlineData("\"Description with quotes\"", true)]
		public void FormatLine_DescriptionWithIllegalValues_ReturnsExpectedData(string description, bool useDataQuote)
		{
			var formatter = new Formatter();
			var rule = new TestRuleWithData { Description = description };
			var ruleData = rule.CreateDocumentation();
			string q = "\"";
			if (useDataQuote)
			{
				q = "";
				description = description.Replace('"', '“');
			}

			string expected = $"TestRule,{q}{description}{q},hello,7{Environment.NewLine}";
			string actual = formatter.SetHeaderKeys(ruleData.GetAllKeys())
				.FormatLine(ruleData, 0);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("Value,withcomma", false)]
		[InlineData("Value\nwith new line", false)]
		[InlineData("Value\rwith return line", false)]
		[InlineData("\"Value with quotes\"", true)]
		public void FormatLine_ValueWithIllegalValues_ReturnsExpectedData(string value, bool useDataQuote)
		{
			var formatter = new Formatter();
			var rule = new TestRuleWithData { Value = value };
			var ruleData = rule.CreateDocumentation();
			string q = "\"";
			if (useDataQuote)
			{
				q = "";
				value = value.Replace('"', '“');
			}

			string expected = $"TestRule,Test Description,{q}{value}{q},7{Environment.NewLine}";
			string actual = formatter.SetHeaderKeys(ruleData.GetAllKeys())
				.FormatLine(ruleData, 0);

			Assert.Equal(expected, actual);
		}

		private class TestRule : IDocumented
		{
			public RuleData CreateDocumentation()
			{
				return RuleData.Construct("TestRule", "Test Description", this);
			}
		}

		private class TestRuleWithData : IDocumented
		{
			public string Name { get; set; } = "TestRule";
			public string Description { get; set; } = "Test Description";
			public string Field1 { get; set; } = "Field1";
			public string Field2 { get; set; } = "Field2";
			public string Value { get; set; } = "hello";

			public RuleData CreateDocumentation()
			{
				return RuleData.Construct(Name, Description, this)
					.AddData(Field2, 7)
					.AddData(Field1, Value);
			}
		}
	}
}
