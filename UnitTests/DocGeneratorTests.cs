//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Collections.Generic;
using UnitTests.FizzBuzzRuleSet;
using UnitTests.FizzBuzzRuleSet.Rules;
using UnitTests.FizzBuzzRuleSet.Conditions.Builder;
using UnitTests.FizzBuzzRuleSet.Conditions.Value;
using UnitTests.FizzBuzzRuleSet.Formatters;
using Xunit;

namespace UnitTests
{
	public class DocGeneratorTests
	{
		[Fact]
		public void GenerateDocs_SingleRule_ReturnsExpectedDocs()
		{
			var rule = new ValueIsMultipleOf(11);
			var docGen = new DocGenerator();

			string[] expected =
			{
				"Name,Description,ConditionType,Input,Output,ReturnValue,RuleType\n",
				"ValueIsMultipleOf,Checks to see if the value is a multiple of 11,Modulus,int,true/false,true if value is a multiple of 11,IConditionValue\n",
			};
			var actual = docGen.FormatRules(rule);

			Assert.Equal(2, actual.Count);
			var iter = actual.GetEnumerator();

			for (int i = 0; i < expected.Length; ++i)
			{
				iter.MoveNext();
				Assert.Equal(expected[i], iter.Current);
			}
		}

		[Fact]
		public void GenerateDocs_ComboRule_ReturnsExpectedDocs()
		{
			var rule = new KeywordRule("Fizz", new ValueIsMultipleOf(3), new Concat<string>());
			var docGen = new DocGenerator();

			string[] expected =
			{
				"Name,,Description,ConditionType,FormatterType,Input,Keyword,Output,Parent,ReturnValue,RuleType\n",
				"Keyword Rule,,Will insert the keyword into the formatter if the condition is met.,,,StringBuilder int,Fizz,StringBuilder,,,IRule with IConditionValue and IFormatter<String>\n",
				",ValueIsMultipleOf,Checks to see if the value is a multiple of 3,Modulus,,int,,true/false,KeywordRule(Fizz),true if value is a multiple of 3,IConditionValue\n",
				",Concat<String>,Appends values to the end of the StringBuilder,,Concat,String,,StringBuilder,KeywordRule(Fizz),,IFormatter<String>\n",
			};
			var actual = docGen.FormatRules(rule);

			Assert.Equal(4, actual.Count);
			for (int i = 0; i < expected.Length; ++i)
			{
				Assert.Equal(expected[i], actual[i]);
			}
		}

		[Fact]
		public void GenerateDocs_ClassWithMultipleRules_ReturnsExpectedDocs()
		{
			var fizzBuzz = new FizzBuzz(new BuilderCheckRule(new BuilderIsEmpty(), new Concat<int>()))
				.AddRule(new KeywordRule("Fizz", new ValueIsMultipleOf(3), new Concat<string>()))
				.AddRule(new KeywordRule("Buzz", new ValueIsMultipleOf(5), new Concat<string>()));
			var docGen = new DocGenerator();

			string[] expected =
			{
				"Name,,,Description,ConditionType,FormatterType,Input,Keyword,Output,Parent,ReturnValue,RuleOrder,RuleType\n",
				"FizzBuzz,,,Rules for processing FizzBuzz,,,,,,,,,Container for IRules\n",
				",Keyword Rule,,Will insert the keyword into the formatter if the condition is met.,,,StringBuilder int,Fizz,StringBuilder,FizzBuzz,,1,IRule with IConditionValue and IFormatter<String>\n",
				",,ValueIsMultipleOf,Checks to see if the value is a multiple of 3,Modulus,,int,,true/false,KeywordRule(Fizz),true if value is a multiple of 3,,IConditionValue\n",
				",,Concat<String>,Appends values to the end of the StringBuilder,,Concat,String,,StringBuilder,KeywordRule(Fizz),,,IFormatter<String>\n",
				",Keyword Rule,,Will insert the keyword into the formatter if the condition is met.,,,StringBuilder int,Buzz,StringBuilder,FizzBuzz,,2,IRule with IConditionValue and IFormatter<String>\n",
				",,ValueIsMultipleOf,Checks to see if the value is a multiple of 5,Modulus,,int,,true/false,KeywordRule(Buzz),true if value is a multiple of 5,,IConditionValue\n",
				",,Concat<String>,Appends values to the end of the StringBuilder,,Concat,String,,StringBuilder,KeywordRule(Buzz),,,IFormatter<String>\n",
				",Builder Check Rule,,Will insert the passed in value if the builder condition is met,,,StringBuilder int,,StringBuilder,FizzBuzz,,3,IRule with IConditionBuilder and IFormatter<int>\n",
				",,BuilderIsEmpty,Checks if the StringBuilder is empty,Builder Length,,StringBuilder,,true/false,BuilderCheckRule,true if the builder is empty,,IConditionBuilder\n",
				",,Concat<Int32>,Appends values to the end of the StringBuilder,,Concat,Int32,,StringBuilder,BuilderCheckRule,,,IFormatter<Int32>\n",
			};
			var actual = docGen.FormatRules(fizzBuzz);

			Assert.Equal(11, actual.Count);
			for (int i = 0; i < expected.Length; ++i)
			{
				Assert.Equal(expected[i], actual[i]);
			}
		}

		private class Container : IDocumented
		{

			public Container(string name)
			{
				Name = name;
			}

			public string Name { get; }

			//public void Process(...)
			//{
			//	// gets rules by some logic
			//	// rule.Process(...)
			//}

			public Container AddRule(SubRule rule)
			{
				rule.SetParent(this);
				_rules.Add(rule.Name, rule);
				return this;
			}

			public RuleData CreateDocumentation()
			{
				var docs = RuleData.Construct(Name, "ContainerDescription", this)
					.AddData("RuleType", "Container");
				foreach (var pair in _rules)
				{
					docs.AddChild(pair.Value)
						.AddData("Parent", $"Container({Name})");
				}
				return docs;
			}

			private readonly Dictionary<string, SubRule> _rules = new Dictionary<string, SubRule>();
		}

		private class SubRule : IDocumented
		{
			public SubRule(string name)
			{
				Name = name;
			}
			public string Name { get; }
			// With IoC, indirect recursion most likely needs a call like this to occur
			// So identifying should be easy.
			public void SetParent(Container container)
			{
				_parent = container;
			}
			//public void Process(...)
			//{
			//	// processes data
			//	// if (shouldRecurse)
			//	// { call back to _parent }
			//}
			public RuleData CreateDocumentation()
			{
				return RuleData.Construct(Name, "SubRuleDescription", this)
					.AddData("RuleType", "SubRule")
					.AddData("Recurse To", "Parent");
				// don't include parent, otherwise we get infinite recursion
			}
			private Container _parent;
		}

		[Fact]
		public void GenerateDoc_RecursiveRules_ReturnsExpectedDocs()
		{
			var container = new Container("Main")
				.AddRule(new SubRule("Rule1"))
				.AddRule(new SubRule("Rule2"))
				.AddRule(new SubRule("Rule3"));

			var docGen = new DocGenerator();

			string[] expected =
			{
				"Name,,Description,Parent,Recurse To,RuleType\n",
				"Main,,ContainerDescription,,,Container\n",
				",Rule1,SubRuleDescription,Container(Main),Parent,SubRule\n",
				",Rule2,SubRuleDescription,Container(Main),Parent,SubRule\n",
				",Rule3,SubRuleDescription,Container(Main),Parent,SubRule\n",
			};
			var actual = docGen.FormatRules(container);

			Assert.Equal(5, actual.Count);
			for (int i = 0; i < expected.Length; ++i)
			{
				Assert.Equal(expected[i], actual[i]);
			}
		}
	}
}
