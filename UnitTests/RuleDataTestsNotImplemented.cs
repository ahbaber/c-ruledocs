﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using RuleDocs;
using System.Collections.Generic;
using Xunit;

namespace UnitTests
{
	public class RuleDataTestsNotImplemented
	{
		private interface ITestRule
		{ }

		private class RuleContainer : IDocumented
		{
			public RuleData CreateDocumentation()
			{
				var docs = RuleData.Construct("RuleContainer", "Contains other rules", this);
				foreach (var rule in _rules)
				{
					docs.AddChild(rule);
				}
				return docs;
			}

			public RuleContainer AddRule(ITestRule rule)
			{
				_rules.Add(rule);
				return this;
			}

			private List<ITestRule> _rules = new List<ITestRule>();
		}

		private class SubruleDocumented : ITestRule, IDocumented
		{
			public RuleData CreateDocumentation()
			{
				return RuleData.Construct("SubruleDocumented", "Rule documentation was implemented", this);
			}
		}

		private class SubruleNoDocs : ITestRule
		{ }

		[Fact]
		public void CreateDocumentation_ReturnsDocumentedRules()
		{
			var ruleContainer = new RuleContainer()
				.AddRule(new SubruleDocumented());
			var docGen = new DocGenerator();

			string[] expected =
			{
				"Name,,Description\n",
				"RuleContainer,,Contains other rules\n",
				",SubruleDocumented,Rule documentation was implemented\n",
			};
			var actual = docGen.FormatRules(ruleContainer);

			Assert.Equal(expected.Length, actual.Count);
			for (int i = 0;  i < expected.Length; ++i)
			{
				Assert.Equal(expected[i], actual[i]);
			}
		}

		[Fact]
		public void CreateDocumentation_ReturnsDocumentedAndUndocumentedRules()
		{
			var ruleContainer = new RuleContainer()
				.AddRule(new SubruleDocumented())
				.AddRule(new SubruleNoDocs());
			var docGen = new DocGenerator();

			string[] expected =
			{
				"Name,,Description,MissingDocType\n",
				"RuleContainer,,Contains other rules,\n",
				",SubruleDocumented,Rule documentation was implemented,\n",
				",Unimplemented,This class has not implemented and documentation,UnitTests.RuleDataTestsNotImplemented+ITestRule\n",
			};
			var actual = docGen.FormatRules(ruleContainer);

			Assert.Equal(expected.Length, actual.Count);
			for (int i = 0;  i < expected.Length; ++i)
			{
				Assert.Equal(expected[i], actual[i]);
			}
		}
	}
}
