Main documentation is on the [Wiki](https://bitbucket.org/ahbaber/c-ruledocs/wiki/Home) now.

#Change Log

2019 Mar 02 V1.0.3

 * Undocumented classes will now generate a documentation line indicating that documentation is missing.

2019 Jan 29 v1.0.2

 * Bug fix: Formatter now applies FormatData() properly to each data element.

2019 Jan 23 v1.0.1

 * Initial Release
